import {DefaultTheme, configureFonts} from "react-native-paper";
import DeltaFonts from "./Fonts";

enum COLORS {
    Grey = '#f4f4f4',
    DarkGrey = '#3D3D3D',
    Black = '#000000',
    White = '#FFFFFF',
    Blue = '#0C71E0',
    Red = 'red',
    Green = 'green',
    LightBlue = '#00e0ff',
    Emoji='#ffcc33'
}

export const theme = {
    ...DefaultTheme,
    fonts: configureFonts(DeltaFonts),
    colors: {
        ...DefaultTheme.colors,
        primary: COLORS.Blue,
        accent: COLORS.White,
        text:COLORS.Blue
    },
    fontSizes:{
        very_small:10,
        small:14,
        medium:18,
        large:22,
        very_large:26
    },
    iconSizes:{
        small:16,
        normal:20,
        large:26
    }
};
