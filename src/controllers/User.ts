import {API_METHODS, API_ENDPOINTS} from "../util/Constants";
import {make_call} from "../services/ApiBase";
import {iUser} from "../interfaces/iUser";

export const get_users = async (page_num) => {

    try {

        let results = await make_call(API_METHODS.GET, API_ENDPOINTS.USERS + `?per_page=50&&page=${page_num}`, false, null);

        return {users_returned: results.data as unknown as Array<iUser>, max_page: results.total_pages};

    } catch (err) {
        throw err;
    }

};
