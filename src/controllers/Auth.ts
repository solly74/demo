import {make_call} from "../services/ApiBase";
import {API_ENDPOINTS, API_METHODS} from "../util/Constants";
import {iUserLogin} from "../interfaces/iUser";
import {user_store} from "../util/LocalStorage";

export const login = async (email: string, password: string) => {

    try {

        let results =  await make_call(API_METHODS.POST, API_ENDPOINTS.LOGIN, false, {
            email,
            password
        }) as unknown as iUserLogin;

        user_store.save_user(results);

        return results;


    } catch (err) {
        console.log("unable to log in", err);
        throw err;
    }

};
