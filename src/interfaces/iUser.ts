export interface iUserLogin {
    token: string
}

export interface iUserStore {
    save_user: (user: iUserLogin) => void,
    get_user: () => Promise<iUserLogin>,
}

export interface iUserLogout {
    logout_user: () => {}
}

export interface iUser {
    id: number,
    email:string,
    first_name:string,
    last_name:string,
    avatar:string
}
