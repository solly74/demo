import {UserReducerTypes as Types} from "../types";

export const initialState = {
    is_logged_in: false
};

const user = (state = initialState, action) => {

    const {type, data} = action;

    switch (type) {

        case Types.login:
            return {...state, is_logged_in: data};

        case Types.logout:
            return initialState;

        default:
            return initialState;

    }

};

export default user;
