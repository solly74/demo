import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers/';
import thunkMiddleware from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

let middleware = applyMiddleware(thunkMiddleware);

const persistConfig = {
    key: 'root',
    storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = createStore(persistedReducer, {}, middleware);
export const persistor = persistStore(store);
