import React from "react";
import {Button, Text, TextInput, View} from "react-native";
import {useDispatch, useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ROUTES} from "./util/Constants";
import Login from "./views/auth/Login";
import Home from "./views/home/Home";
import DeltaButton from "./components/buttons/DeltaButton";
import {initiate_logout_user} from "./util/LocalStorage";

//set up screen navigation
const App = createStackNavigator();
const AppStack = () => {

    const dispatch = useDispatch();
    const user = useSelector(state => state.user);
    const logout_user = initiate_logout_user(dispatch).logout_user;

    return <App.Navigator initialRouteName={user.is_logged_in ? ROUTES.home : ROUTES.login}>
        <App.Screen name={ROUTES.login} component={Login}/>
        <App.Screen name={ROUTES.home} component={Home} options={{
            headerRight: () => (
                <DeltaButton title={'Logout'} mode={'text'} onPress={logout_user}/>
            )
        }}/>
    </App.Navigator>

};


function AppInit() {

    return <NavigationContainer>
        <AppStack/>
    </NavigationContainer>
}

export default AppInit;
