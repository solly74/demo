import React, {useState, useEffect} from "react";
import {Text, useTheme} from "react-native-paper";

function DeltaText(props) {

    const {fontSizes} = useTheme();

    const styles = {

        text_input: {
            maxHeight: 50,
            width: '100%',
            marginBottom: 10,
            fontSize: fontSizes.medium
        },

    };

    return <Text style={styles.text_input}>
        {props.children}
    </Text>
}

export default DeltaText;
