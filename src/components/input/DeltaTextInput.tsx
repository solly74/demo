import React, {useState, useEffect} from "react";
import {TextInput,useTheme} from "react-native-paper";

function DeltaTextInput(props) {

    const {colors} = useTheme();

    const styles = {

        text_input: {
            maxHeight: 50,
            width: '100%',
            marginBottom: 10,

        },

    };

    return <TextInput mode={'outlined'} label={props.placeholder} style={styles.text_input}
    onChangeText={props.onChangeText}
    placeholderTextColor={colors.primary} placeholder={props.placeholder}
    secureTextEntry={props.secureTextEntry}/>
}

export default DeltaTextInput;
