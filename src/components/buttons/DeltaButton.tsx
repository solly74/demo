import React, {useState} from "react";
import {View} from "react-native";
import {Button, useTheme} from "react-native-paper";

function DeltaButton(props) {
    const [loading, setLoading] = useState(false);
    const {colors} = useTheme();

    const styles = {

        parent: {
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            width:'100%'
        },

    };

    const on_press = async () => {
        setLoading(true);
        try {
            await props.onPress();
            setLoading(false);
        }
        catch(err){
            setLoading(false);
        }


    };

    return <Button mode={props.mode ? props.mode : 'contained'} color={colors.button_accept} loading={loading}
                   style={[styles.parent, props.style]}
                   onPress={on_press} disabled={props.disabled}>

        {props.title}

    </Button>
}


export default DeltaButton;

