import {API_ENDPOINTS, API_RESPONSES, ENVIRONMENT} from "../util/Constants";
import {iUserLogin, iUserStore} from "../interfaces/iUser";

const timeout = 15000;

export const make_call = async (method, action, auth, data, user_store?: iUserStore) => {

    try {

        //handle timeout set to 15 seconds
        const abortController = AbortController ? new AbortController() : null;
        setTimeout(() => {
            abortController.abort();
        }, timeout);

        //build the headers
        let headers = {};

        let user_access_token = null;

        if (user_store) {
            let user: iUserLogin = await user_store.get_user();
            user_access_token = user.token;
        }
//if auth is required add auth token
        if (auth) {
            if (user_access_token) {
                headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + user_access_token
                };
            } else {
                throw 'no user access token';
            }
        } else {
            headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            };
        }

        //build the url
        let home_url: string = ENVIRONMENT.is_production ? API_ENDPOINTS.BASE_URL : API_ENDPOINTS.BASE_URL_STAGING;

        let url: string = home_url + action;

        console.log('trying to call ' + url + ' with a ' + method + ' headers are ' + JSON.stringify(headers) + ' data: ' + JSON.stringify(data));

        //make the call
        let response = await fetch(url, {
            method: method,
            headers: headers,
            signal: abortController ? abortController.signal : null,
            body: data ? JSON.stringify(data) : null,
        });

        return check_response(response);


    } catch (err) {
        throw err;
    }

};

const check_response = async response => {

    let response_formatted: string = await response.json();

    console.log(response_formatted);

    if (response.status === 200 || response.status === 201) {
        return response_formatted;
    } else if (response.status === 401 || response.status === 400) {
        throw API_RESPONSES.unauthenticated;
    } else {
        throw `error from API status is ${response.status}`;
    }
};
