import {Platform} from 'react-native';

export const ENVIRONMENT = ({
    is_production: true,
    is_ios: Platform.OS === 'ios',
    build: '1.0.0'
});

export const API_ENDPOINTS = {

    BASE_URL: 'https://reqres.in/api/',
    BASE_URL_STAGING: 'https://reqresstaging.in/api/',
    LOGIN: 'login',
    USERS: 'users'
};

export enum API_METHODS {
    GET = 'GET',
    POST = 'POST',
    PATCH = 'PATCH',
    DELETE = 'DELETE',
    PUT = 'PUT'
}

export enum API_RESPONSES {
    unauthenticated = 'unauthenticated'
}

export enum ROUTES {
    login = 'Login',
    home = 'Home',
}

export enum ASYNC_OBJECTS {

    user = 'user'

}

