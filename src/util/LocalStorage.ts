import EncryptedStorage from 'react-native-encrypted-storage';
import {iUserLogin, iUserLogout, iUserStore} from "../interfaces/iUser";
import {ASYNC_OBJECTS} from "./Constants";
import {UserReducerTypes} from "../redux/types";

async function save_item(key, value) {

    return await EncryptedStorage.setItem(key, value);

}

async function get_item(key): Promise<string> {

    return await EncryptedStorage.getItem(key);

}

async function remove_item(key) {
    return await EncryptedStorage.removeItem(key);
}

//region USER

export const user_store: iUserStore = {

    async save_user(user: iUserLogin) {
        try {

            await save_item(ASYNC_OBJECTS.user, JSON.stringify(user));


        } catch (err) {
            console.log("Unable to save user to local", err);
            throw err;
        }
    },

    async get_user() {
        try {

            let user_object = await get_item(ASYNC_OBJECTS.user);

            return await JSON.parse(user_object) as iUserLogin;

        } catch (err) {
            console.log("Unable to get user from local", err);
            throw err;
        }
    }
};

export function initiate_logout_user(dispatch): iUserLogout {

    return {

        async logout_user() {

            try {
                await EncryptedStorage.clear();
                dispatch({type: UserReducerTypes.logout});
            } catch (err) {
                console.log("Unable to delete user from local", err);
                throw err;
            }
        }
    }
}


//endregion
