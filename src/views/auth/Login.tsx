import React, {useState} from "react";
import {View, Button, TextInput, Text, Alert} from "react-native";
import DeltaTextInput from "../../components/input/DeltaTextInput";
import DeltaButton from "../../components/buttons/DeltaButton";
import DeltaText from "../../components/labels/DeltaLabel";
import {iUserLogin} from "../../interfaces/iUser";
import {login} from "../../controllers/Auth";
import {useDispatch} from "react-redux";
import {UserReducerTypes} from "../../redux/types";
import {useNavigation, CommonActions} from '@react-navigation/native';
import {API_RESPONSES, ROUTES} from "../../util/Constants";
import {iNavigation} from "../../interfaces/iNavigation";

function Login() {
    const dispatch = useDispatch();
    const navigation: iNavigation = useNavigation();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const login_user = async () => {
        try {
            let user_login: iUserLogin = await login(username, password);
            console.log("user is logged in?", user_login);
            dispatch({type: UserReducerTypes.login, data: true});
            navigate_user_home();

        } catch (err) {

            switch (err) {
                case API_RESPONSES.unauthenticated:
                    Alert.alert('Oops', 'Please ensure you enter the correct email and password');
                    break;

                default:
                    Alert.alert('Oops', 'Unable to login at this time');
            }

        }

    };

    const navigate_user_home = () => {

        navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [
                    {name: ROUTES.home},
                ],
            }),
        );
    };

    return <View style={styles.parent}>
        <View style={styles.content}>
            <DeltaText>Hello, please sign in</DeltaText>
            <DeltaTextInput placeholder={'Username'} onChangeText={text => setUsername(text)}/>
            <DeltaTextInput placeholder={'Password'} secureTextEntry onChangeText={text => setPassword(text)}/>
            <View style={styles.button}>
                <DeltaButton title={'Login'} onPress={login_user}/>
            </View>
        </View>
    </View>
}

const styles = {

    parent: {
        flex: 1,
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

    content: {
        flex: 0.35,
        width: '100%',
        justifyContent: 'space-between',
    },

    button: {
        height: 60,
    }

};

export default Login;
