import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";
import {View, FlatList, Image, Alert, Dimensions} from "react-native";
import {Text, useTheme, ActivityIndicator} from "react-native-paper";
import {iNavigation} from "../../interfaces/iNavigation";
import {ROUTES} from "../../util/Constants";
import {useNavigation, CommonActions} from '@react-navigation/native';
import {get_users} from "../../controllers/User";

function Home() {

    const user = useSelector(state => state.user);
    const [loading, setLoading] = useState(false);
    const [users, setUsers] = useState([]);
    const [page, setPage] = useState(1);
    const [maxPage, setMaxPage] = useState(1);
    const {colors} = useTheme();

    //use effect hook to monitor users length
    //if users list is empty then reset page values and fetch users from api
    useEffect(() => {

        if (users.length < 1) {
            setPage(1);
            setMaxPage(1);
            get_users_from_api();
        } else {
            console.log("users are not empty", users);
        }

    }, [users]);

    // use effect hook for the state value page
    // every time page value changes the get users from api is called with new page value
    useEffect(() => {
        console.log("page is", page);
        get_users_from_api();

    }, [page]);

    if (!user.is_logged_in) {
        logout();
    }

    //get users from api and concat into current users list
    const get_users_from_api = async () => {
        setLoading(true);
        try {
            if (page <= maxPage) {
                let {users_returned, max_page} = await get_users(page);
                setUsers(users.concat(users_returned));
                setMaxPage(max_page);
            }

            setLoading(false);

        } catch (err) {
            Alert.alert('Oops', 'Unable to get users right now');
            setLoading(false);
        }
    };

    const refresh = async () => {

        await setUsers([]);

    };

    const on_end_reached = () => {

        setPage(page + 1);

    };

    const render_item = ({item}) => {
        return <View key={item.id} style={styles.card}>
            <Image source={{uri: item.avatar}} style={styles.profile_picture}/>
            <Text>{item.email}</Text>
            <Text>{`${item.first_name} ${item.last_name}`}</Text>
        </View>
    };

    const styles = {

        parent: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },

        list: {
            width: "100%",
            height: Dimensions.get('window').height
        },

        profile_picture: {
            width: 60,
            height: 60,
            borderRadius: 60
        },

        card: {
            height: 180,
            backgroundColor: colors.accent,
            margin: 10,
            borderRadius: 5,
            borderWidth: 1,
            borderColor: colors.placeholder,
            padding: 10,
            alignItems: 'center',
            justifyContent: 'space-between'
        }

    };

    return <View style={styles.parent}>
        <FlatList style={styles.list} data={users} renderItem={render_item} refreshing={loading}
                  onRefresh={refresh} onEndReached={on_end_reached} onEndReachedThreshold={0.1}
                  ListFooterComponent={(
                      <ActivityIndicator size={'small'} color={colors.primary} animating={loading}/>)}/>
    </View>
}

const logout = () => {
    const navigation: iNavigation = useNavigation();

    navigation.dispatch(
        CommonActions.reset({
            index: 1,
            routes: [
                {name: ROUTES.login},
            ],
        }),
    );
};

export default Home;
