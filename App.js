import React from 'react';

import MainApp from './src/App';
import {Provider as StoreProvider} from 'react-redux';
import {Provider as PaperProvider} from 'react-native-paper';
import {store, persistor} from './src/redux/store';
import {PersistGate} from 'redux-persist/integration/react';
import {theme} from './src/theming/Theme';

export default function () {
    return <StoreProvider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <PaperProvider theme={theme} children={null}>
                <MainApp/>
            </PaperProvider>
        </PersistGate>
    </StoreProvider>;
}

